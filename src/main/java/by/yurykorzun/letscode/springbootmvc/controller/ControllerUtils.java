package by.yurykorzun.letscode.springbootmvc.controller;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.Map;
import java.util.stream.Collectors;

public class ControllerUtils {

    static Map<String, String> getErrors(BindingResult bindingResult) {
        Map<String, String> errors = bindingResult.getFieldErrors().stream().collect(Collectors.toMap(
                        fieldError -> fieldError.getField() + "Error",
                        FieldError::getDefaultMessage,
                        (k1, k2)-> { System.out.println("duplicate key" + k1); return k1; } // ignore duplicate keys
                ));
        return errors;
    }
}
