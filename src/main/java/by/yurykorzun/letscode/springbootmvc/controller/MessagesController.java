package by.yurykorzun.letscode.springbootmvc.controller;

import by.yurykorzun.letscode.springbootmvc.entities.Message;
import by.yurykorzun.letscode.springbootmvc.entities.User;
import by.yurykorzun.letscode.springbootmvc.repository.MessagesRepository;
import by.yurykorzun.letscode.springbootmvc.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

@Controller
public class MessagesController {

    private MessagesRepository messagesRepository;
    private ImageService imageService;

    @Value("${upload.path}")
    private String uploadPath;

    @Autowired
    public MessagesController(MessagesRepository repository, ImageService imageService){
        this.messagesRepository = repository;
        this.imageService = imageService;
    }

    @GetMapping("messages")
    public String userMessages(@RequestParam(required = false, defaultValue = "") String tag,
                               Model model){
        Iterable<Message> messages = (tag != null && !tag.isEmpty())
                ? messagesRepository.findAll()
                : messagesRepository.findByTagLike("%" + tag + "%");
        model.addAttribute("messages", messages);
        model.addAttribute("tag", tag);
        return "messages";
    }

    @PostMapping("messages")
    public String addMessage(
            @AuthenticationPrincipal User user,
            @Valid Message message,
            BindingResult bindingResult,
            Model model,
            @RequestParam MultipartFile file) {

        message.setAuthor(user);
        if (bindingResult.hasErrors()){
            Map<String, String> errors = ControllerUtils.getErrors(bindingResult);
            model.mergeAttributes(errors);
            model.addAttribute("message", message);
        } else {
            setMessageFile(message, file);

            messagesRepository.save(message);
            model.addAttribute("message", null);
        }

        Iterable<Message> messages = messagesRepository.findAll();
        model.addAttribute("messages", messages);
        return "messages";
    }

    private void setMessageFile(@Valid Message message, @RequestParam MultipartFile file) {
        if (file != null && !file.isEmpty()){
            File uploadDir = new File(uploadPath);
            if (!uploadDir.exists()) uploadDir.mkdir();

            String uniqueFilename = UUID.randomUUID().toString() + "." + file.getOriginalFilename();
            message.setFilename(uniqueFilename);

            imageService.resizeAndSave(file, uploadPath + "/" + uniqueFilename);
        }
    }


    @GetMapping("/users/{user}/messages")
    public String userMessages(@AuthenticationPrincipal User userAuth,
                               @PathVariable("user") User userRequested,
                               @RequestParam(value = "message", required = false) Message message,
                               Model model){
        Set<Message> messages = userRequested.getMessages();
        model.addAttribute("isCurrentUser", userAuth != null && userAuth.equals(userRequested));
        model.addAttribute("message", message);
        model.addAttribute("messages", messages);
        return "userMessages";
    }

    @PostMapping("/users/{user}/messages")
    public String saveMessage(@AuthenticationPrincipal User userAuth,
                              @PathVariable("user") User userRequested,
                              @RequestParam(value = "message", required = false) Message message,
                              @RequestParam String text,
                              @RequestParam String tag,
                              @RequestParam MultipartFile file,
                              Model model){
        if (userAuth != null && userAuth.equals(userRequested)){

            if (!file.isEmpty()) setMessageFile(message, file);
            if (!StringUtils.isEmpty(text)) message.setText(text);
            if (!StringUtils.isEmpty(tag)) message.setTag(tag);
            messagesRepository.save(message);
        }
        Set<Message> messages = userRequested.getMessages();
        model.addAttribute("isCurrentUser", userAuth != null && userAuth.equals(userRequested));
        model.addAttribute("message", message);
        model.addAttribute("messages", messages);
        return "userMessages";
    }

}
