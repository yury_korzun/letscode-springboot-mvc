package by.yurykorzun.letscode.springbootmvc.controller;

import by.yurykorzun.letscode.springbootmvc.entities.User;
import by.yurykorzun.letscode.springbootmvc.entities.dto.CaptchaResponseDto;
import by.yurykorzun.letscode.springbootmvc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.util.Collections;
import java.util.Map;

@Controller
public class RegistrationController {

    private UserService userService;
    private RestTemplate restTemplate;

    @Value("${google.recaptcha.secret}")
    private String recaptchaSecret;

    @Value("${google.recaptcha.url}")
    private String recaptchaUrl;

    @Autowired
    public RegistrationController(UserService userService, RestTemplate restTemplate){
        this.userService = userService;
        this.restTemplate = restTemplate;
    }

    @GetMapping("/registration")
    public String showRegistration(){
        return "registration";
    }

    @PostMapping("/registration")
    public String register(
            @RequestParam("passwordConfirmation") String passwordConfirmation,
            @RequestParam("g-recaptcha-response") String recaptchaResponse,
            @Valid User user, BindingResult bindingResult, Model model){

        // default message
        InfoMessage.addToModel( model, "Wrong input! Check info below.", InfoMessage.Type.ERROR);

        // pass confirm check
        boolean passConfirmFail =  StringUtils.isEmpty(user.getPassword())
                                || !user.getPassword().equals(passwordConfirmation);
        if (passConfirmFail){
            model.addAttribute("passwordConfirmationError", "Confirmation fail");
        }

        // try to create user
        boolean userCreateFail = !userService.addUser(user);
        if (userCreateFail){
            InfoMessage.addToModel( model, "User exists!", InfoMessage.Type.ERROR);
        }

        // check recaptcha
        String url = String.format(recaptchaUrl, recaptchaSecret, recaptchaResponse);
        CaptchaResponseDto captchaResponseDto =
                restTemplate.postForObject(url, Collections.emptyList(), CaptchaResponseDto.class);
        if (!captchaResponseDto.isSuccess()){
            model.addAttribute("captchaError", "FIll captcha");
        }

        if (bindingResult.hasErrors() || userCreateFail || passConfirmFail || !captchaResponseDto.isSuccess()){
            Map<String, String> errors = ControllerUtils.getErrors(bindingResult);
            model.mergeAttributes(errors);
            model.addAttribute("user", user);
            return "registration";
        }

        InfoMessage.addToModel( model, "User registered! Please, log in!", InfoMessage.Type.SUCCESS);

        return "redirect:/login";
    }

    @GetMapping("/activate/{code}")
    public String activate(@PathVariable("code") String activationCode,
                           Model model){
        if (userService.activateUser(activationCode)){
            InfoMessage.addToModel(model, "User activated! Please login!", InfoMessage.Type.SUCCESS);
        } else {
            InfoMessage.addToModel(model, "Wrong activation code!", InfoMessage.Type.ERROR);
        }
        return "login";
    }

}
