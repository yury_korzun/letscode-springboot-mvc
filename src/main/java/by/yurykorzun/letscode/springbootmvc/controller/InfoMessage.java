package by.yurykorzun.letscode.springbootmvc.controller;

import lombok.Getter;
import org.springframework.ui.Model;

@Getter
public class InfoMessage {
    enum Type{
        SUCCESS,
        ERROR {
            @Override
            String bootstrapClass() {
                return "danger";
            }
        };
        String bootstrapClass() {
            return name().toLowerCase();
        }
        @Override
        public String toString() {
            return bootstrapClass();
        }
    }
    private String text;
    private Type type;

    private InfoMessage(){};
    public static InfoMessage of(String text, Type type){
        InfoMessage message = new InfoMessage();
        message.text = text;
        message.type = type;
        return message;
    }
    public static void addToModel(Model model, String text, Type type){
        model.addAttribute("message", InfoMessage.of(text, type));
    }

    @Override
    public String toString() {
        return text;
    }
}
