package by.yurykorzun.letscode.springbootmvc.repository;

import by.yurykorzun.letscode.springbootmvc.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
    User findByActivationCode(String code);
}
