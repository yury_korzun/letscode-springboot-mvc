package by.yurykorzun.letscode.springbootmvc.repository;

import by.yurykorzun.letscode.springbootmvc.entities.Message;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MessagesRepository extends CrudRepository<Message, Long> {
    List<Message> findByTagLike(String tag);
}
