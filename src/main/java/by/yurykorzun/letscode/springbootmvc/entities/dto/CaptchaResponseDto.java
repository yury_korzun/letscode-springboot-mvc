package by.yurykorzun.letscode.springbootmvc.entities.dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter @Setter
public class CaptchaResponseDto {

    private boolean success = false;

    @JsonAlias("error-codes")
    private Set<String> errorCodes;

    public CaptchaResponseDto(){}
}
