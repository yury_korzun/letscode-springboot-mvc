package by.yurykorzun.letscode.springbootmvc.entities;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Data
public class Message {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User author;

    @NotBlank(message = "Please fill the message")
    @Length(min = 10, max = 255, message = "Message must be 10-255 chars long")
    private String text;

    @NotBlank(message = "Please fill the tag")
    @Length(max = 255, message = "tag must be 10-255 chars long")
    private String tag;

    private String filename;

    public Message(){}

    public Message(User author, String text, String tag) {
        this.author = author;
        this.text = text;
        this.tag = tag;
    }


}
