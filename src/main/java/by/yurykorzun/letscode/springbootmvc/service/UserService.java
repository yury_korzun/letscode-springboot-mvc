package by.yurykorzun.letscode.springbootmvc.service;

import by.yurykorzun.letscode.springbootmvc.entities.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;
import java.util.Map;

public interface UserService extends UserDetailsService {
    boolean addUser(User user);
    boolean activateUser(String activationCode);
    List<User> findAll();
    void saveUser(User user, String username, Map<String, String> form);
    void updateUser(User user, String password, String email);
}
