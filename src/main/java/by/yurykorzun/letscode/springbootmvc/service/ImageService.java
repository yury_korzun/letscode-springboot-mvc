package by.yurykorzun.letscode.springbootmvc.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface ImageService {
    void resizeAndSave(MultipartFile file, String destinationPath);
}
