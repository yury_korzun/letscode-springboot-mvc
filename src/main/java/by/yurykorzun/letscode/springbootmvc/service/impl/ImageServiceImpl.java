package by.yurykorzun.letscode.springbootmvc.service.impl;

import by.yurykorzun.letscode.springbootmvc.service.ImageService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;

@Service
public class ImageServiceImpl implements ImageService {

    private static final int maxSizePx = 100;

    @Override
    public void resizeAndSave(MultipartFile image, String destinationPath) {
        BufferedImage inputImage = null;
        try {
            inputImage = ImageIO.read(image.getInputStream());
            int imageHeight = inputImage.getHeight();
            int imageWidth = inputImage.getWidth();
            int imageMaxSizePx = Math.max(imageHeight, imageWidth);
            float scaleProportion = ((float)maxSizePx) / imageMaxSizePx;
            if (scaleProportion < 1) {
                int scaledWidth =  Math.round(imageWidth  * scaleProportion);
                int scaledHeight = Math.round(imageHeight * scaleProportion);
                BufferedImage outputImage = new BufferedImage(scaledWidth, scaledHeight, inputImage.getType());
                // scale
                Graphics2D g2d = outputImage.createGraphics();
                g2d.drawImage(inputImage, 0, 0, scaledWidth, scaledHeight, null);
                g2d.dispose();
                // write to output file
                String formatName = destinationPath.substring(destinationPath.lastIndexOf(".") + 1);
                ImageIO.write(outputImage, formatName, new File(destinationPath));
            } else {
                image.transferTo(new File(destinationPath));
            }
        } catch (IOException e) {
            try {
                image.transferTo(new File(destinationPath));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
