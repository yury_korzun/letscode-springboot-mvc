package by.yurykorzun.letscode.springbootmvc.service;

public interface MailService {
    void sendEmail(String emailTo, String subject, String message);
}
