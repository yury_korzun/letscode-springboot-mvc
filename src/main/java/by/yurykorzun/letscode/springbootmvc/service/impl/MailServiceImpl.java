package by.yurykorzun.letscode.springbootmvc.service.impl;

import by.yurykorzun.letscode.springbootmvc.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class MailServiceImpl implements MailService {

    private JavaMailSender mailSender;

    @Value("${spring.mail.username}")
    private String mailUsername;

    @Autowired
    public MailServiceImpl(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    @Override
    public void sendEmail(String emailTo, String subject, String message) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom(mailUsername);
        mailMessage.setSubject(subject);
        mailMessage.setTo(emailTo);
        mailMessage.setText(message);
        mailSender.send(mailMessage);
    }
}
