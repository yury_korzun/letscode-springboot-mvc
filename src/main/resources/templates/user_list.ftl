<#import "parts/common.ftl" as c>
<#import "parts/login.ftl" as l>

<@c.page>
    <@l.logout />
    List of users
    <table>
        <thead>
            <th>Name</th>
            <th>Role</th>
            <th></th>
        </thead>
        <tbody>
            <#list users as user>
                <tr>
                    <td>${user.username}</td>
                    <td><#list user.roles as role>${role}<#sep >, </#list></td>
                    <td><a href="/users/${user.id}">Edit</a></td>
                </tr>
            </#list>
        </tbody>
    </table>
</@c.page>