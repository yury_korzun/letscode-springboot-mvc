<#import "parts/common.ftl" as c>
<#import "parts/login.ftl" as l>

<@c.page>
    <@l.logout />
    <form method="post" action="/users">
        <p>User <b>${user.username}</b></p>
        <input type="text" name="username" placeholder="username" value="${user.username}">
        <#list roles as role>
            <div>
                <label><input type="checkbox"
                              name="${role}"
                            ${user.roles?seq_contains(role)?string("checked", "")}
                    >${role}</label>
            </div>
        </#list>
        <input type="hidden" name="userId" placeholder="id" value="${user.id}">
        <input type="hidden" name="_csrf" value="${_csrf.token}" />

        <button type="submit">Save</button>
    </form>
</@c.page>