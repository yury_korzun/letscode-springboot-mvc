#!/usr/bin/env bash

mvn clean package

echo 'Copy files...'

scp -i ~/.ssh/id_rsa_drucoder \
    target/sweater-1.0-SNAPSHOT.jar \
    user@192.168.1.100:/home/user/

echo 'Restart server...'

ssh -i ~/.ssh/id_rsa/id_rsa_drucoder \
    user@192.168.1.100:/home/user/ << EOF

pgrep java | xargs kill -9
nohup java -jar sweater-1.0-SNAPSHOT.jar > log.txt &

EOF

echo 'Finished'